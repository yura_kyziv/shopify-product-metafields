<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('products', function (Request $request) {
    /** @var AuthSession */
    $session = $request->get('shopifySession'); // Provided by the shopify.auth middleware, guaranteed to be active

    $result = \Shopify\Rest\Admin2023_04\Product::all($session);

    return $result;
})->middleware('shopify.auth');

Route::get('product/{id}', function (Request $request, $id) {
    /** @var AuthSession */
    $session = $request->get('shopifySession'); // Provided by the shopify.auth middleware, guaranteed to be active

    $result = \Shopify\Rest\Admin2023_04\Product::find($session, $id);

    return $result;
})->middleware('shopify.auth');
