import {Button, Form, FormLayout, Frame, Page, Text, TextField} from "@shopify/polaris";
import { useParams } from "react-router-dom";
import {useAppQuery} from "../../hooks/index.js";
import {useCallback, useState} from "react";
import {Loading} from "@shopify/app-bridge-react";

export default function ProductEdit() {
    const { id } = useParams();

    const { data, isLoading, isError, error } = useAppQuery({
        url: `../../api/product/${id}` ,
        reactQueryOptions: {
            select: (data) => {
                return data;
            },
        }
    });
    const product = data;
    console.log(product);

    const [metafield, setMetafield] = useState('');

    const handleSubmit = useCallback(() => {
        setMetafield('');
    }, []);

    const handleMetafieldChange = useCallback((value) => setMetafield(value), []);

    if(!isLoading){
        return (
            <Page>
                <Text as={"p"}>
                    Product name: {product?.title}
                </Text>
                <Form onSubmit={handleSubmit}>
                    <FormLayout>
                        <TextField
                            value={metafield}
                            onChange={handleMetafieldChange}
                            label="Metafield"
                            type="metafield"
                            autoComplete="metafield"
                            helpText={
                                <span>
                              Add metafield for product.
                            </span>
                            }
                        />
                        <Button submit>Submit</Button>
                    </FormLayout>
                </Form>
            </Page>
        );
    } else {
        return (
            <div style={{height: '100px'}}>
                <Frame>
                    <Loading />
                </Frame>
            </div>
        );
    }
}
