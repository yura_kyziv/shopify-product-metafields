import {
    LegacyCard,
    IndexTable,
    Text, useIndexResourceState, Link, Page,
} from '@shopify/polaris';
import React from 'react';
import { useAppQuery } from "../hooks";

export default function HomePage() {

    const { data, isLoading, isError, error } = useAppQuery({
        url: 'api/products',
        reactQueryOptions: {
            select: (data) => {
                console.log(data)
                //requestData = data
                return data;
            },
            onSuccess: (response) => {
                //requestData = response
                console.log(response)
            }
        }
    });


    const resourceName = {
        singular: 'product',
        plural: 'products',
    };
    console.log(data);
    const {selectedResources, allResourcesSelected, handleSelectionChange} =
        useIndexResourceState(data);
    console.log(data);

    const rowMarkup = data?.map(
        (
            {id, title},
            index
        ) => (
            <IndexTable.Row
                id={id}
                key={id}
                selected={selectedResources.includes(id)}
                position={index}
            >
                <IndexTable.Cell>
                    <Link url={"/products/" + id} external={false}>
                        <Text variant="bodyMd" fontWeight="bold" as="span">
                            {id}
                        </Text>
                    </Link>
                </IndexTable.Cell>
                <IndexTable.Cell>
                    <Text variant="bodyMd" fontWeight="bold" as="span">
                        {title}
                    </Text>
                </IndexTable.Cell>
            </IndexTable.Row>
        ),
    );

    if (isLoading){
        console.log(data);
        return <h1>Loading...</h1>
    }

    if(isError){
        return <h1>error.message</h1>
    }
    return (
        <Page>
            <LegacyCard>
                <IndexTable
                    resourceName={resourceName}
                    itemCount={5}
                    headings={[
                        {title: '#'},
                        {title: 'Title'},
                    ]}
                    selectable={false}
                >
                    {rowMarkup}
                </IndexTable>
            </LegacyCard>
        </Page>
    );
}